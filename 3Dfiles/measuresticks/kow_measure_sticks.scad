// Variable length measuring widget with ticks all along at x increments, with a labelled circular thumb spot for holing it.

// variables 
scl = 254;      // X/Y scaling factor for the object. Openscad base is mm, so scl = 1 will be mm. 10 = cm. 254 = inches.
ot = 3.6;     //object thickness in mm. do multiples of 0.2mm for best layer compatibility. will not scale as above
ol = 7;     //object length in units as per scl
ow = 0.3;      //object width in units as per scl
ff = "Roboto:style=Bold"; // font's font and style
ti = 1;      //tick increments - how far along the widget, in scl, for each tick mark
pvnv = 0;   // positive lettering on top of the widget = 1, negative lettering cut out of the top of the widget = 0

// calculated for optimals - do not manually adjust
label = str(ol); //label - the text on the thumb spot
ts = ol*ow/5;      //thumb size - radius of the circle as per scl, at the thumb grip. Also used for aligning objects on it.
fs = ts;      //font size
dd = ot/3;     //detail depth
tw = (ti*ow)/ol; //tick width - how thick the ticks are at the fat end of the arrow 

// begin stick

scale ([scl,scl,1])
difference()
{

union()  
  { // positive extrustions
    // main body
    linear_extrude(height = ot ) polygon(points=[[0,0],[ol,0],[ol-ow,ow],[ow,ow] ]);
    
    // thumb grip
    translate ([ol/2,ts,0])     cylinder(ot,ts,ts,$fn=6);
    
    // top marks if pvpnv = 1
    translate ([ol/2,ts,ot]) 
    linear_extrude(height = pvnv*dd) text(label,fs, halign = "center", valign = "center", font = ff, $fn=500);
 
    for (t=[ti:ti:ol-ti])
    translate ([t,0,ot])
    linear_extrude(height = pvnv*dd) polygon(points=[[0,0],[tw,(2*ow)/3],[-1*(tw),(2*ow)/3]]);
    
  }



// negative extrusions - sink a smidge below the plane to ensure rendering cut outs correctly
union()
  {
    // top marks
    translate ([ol/2,ts,ot-dd]) 
    linear_extrude(height = (1-pvnv)*dd) text(label,fs, halign = "center", valign = "center", font = ff, $fn=500);
 
    for (t=[ti:ti:ol-ti])
    translate ([t,0,ot-dd])
    linear_extrude(height = dd) polygon(points=[[0,0],[tw,(2*ow)/3],[-1*(tw),(2*ow)/3]]);
    
    //bottom marks
    translate ([ol/2,ts,dd]) rotate([0,180,0])  
    linear_extrude(height = (1-pvnv)*dd) text(label,fs, halign = "center", valign = "center", font = ff, $fn=500);

    for (t=[ti:ti:ol-ti])
    translate ([t,0,0])
    linear_extrude(height = dd) polygon(points=[[0,0],[tw,2*ow/3],[-1*tw,2*ow/3]]);
  }
}