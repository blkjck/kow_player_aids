// Kings of War corner markers

sf = 1; //scale factor. set to 1 for mm and 25.4 for inches. 

ifs = 3.5; // Increments Number/text values font size
txtfnt = "Memo Std"; // Increments Number/text values font. Make sure to set it to something installed on your PC
vp0 = "2024 RISING"; // token top text, victory point value being the usual for tokens
th = 3.2 ; // token (object) height (thickness) value
td = 5 ; // token depth ie thickness of bars
vpd = 0.6 ; // token text and image depth
puff = 2.5; // radius of offset function for making widget have rounded top corners
bw = 30;  // base width of corner marker
wd = 30; // corner depth (height).
thicc = 0.2; // make the logo lines thicc
res = 200; // dimensional accuracy - larger is more fine detail 50 is small, 800 is large

// use tid only if only using one svg image. 
// use both if you want to stack two overlapping images at two depths.
// when stacking two, tis should be the bigger image, and tid the image you want to cut out of it
tid = "../icons/citysilhouette_flipped.svg";; // deep token image, or text. Use a path for image, or string for text. 
                      // You will need to change the command below to either 'import' or 'text', as appropriate.
tis = "../icons/sun.svg"; // shallow token image on the bottom

// Calculated values - DO NOT EDIT DIRECTLY
vpd2 = 0.5*vpd; // secondary token image depth
ho = (puff/2^0.5)-(puff-(puff/2^0.5)); // horizontal offset of puff to align with main edge. lots of trigonometry to get this formula!


//global operations
scale([sf,sf,sf])
  
//HINDERED RISING
rotate([65,-15,15])
difference()
{
  //start of initial object
  union()
    {
      // puffed bars, to give rounded corners on top
      linear_extrude(height = th)
      offset(r = puff, $fn=res)
      polygon(points=[ [0,0],[bw,0],[bw,td],[td,td],[td,wd],[0,wd] ]);
      
    }
  //end of initial object
    
  //start of object to remove
  union()
  {
    rotate([0,0,90])
    translate([-puff*0.35,-puff*0.35,th-vpd])
    linear_extrude(height = vpd)
       {
        text(vp0, font = txtfnt, size = ifs, valign = "top", halign = "left", $fn=res); // top text
       }
    translate([bw/1.5,0,th-vpd])
    scale([0.9,0.9,1])
    linear_extrude(height = vpd)
       {
        import(tid, center = true, $fn=res);                    // deep logo
       }
    translate([bw/1.5,2*puff,th-vpd2])
    scale([0.9,0.9,1])
    linear_extrude(height = vpd2)
       { offset(r = thicc, $fn=res)
         import(tis, center = true, $fn=res);                    // shallow logo 
       }
   // HINDERED
    translate([puff,bw/1.7,0])
    linear_extrude(height = vpd)
    rotate([0,180,90])
       {
        text("HINDERED", font = txtfnt, size = ifs, valign = "center", halign = "center", $fn=res); // top
       }
    translate([bw/1.7,puff,0])
    linear_extrude(height = vpd)
    rotate([0,180,180])
       {
        text("HINDERED", font = txtfnt, size = ifs, valign = "center", halign = "center", $fn=res); // top
       }
/*    translate([bw/1.5,0,0])
   scale([0.9,0.9,1])
    linear_extrude(height = vpd)
    rotate([0,180,0])
       {
        import(tid, center = true, $fn=res);        // deep logo
       }
    translate([bw/1.5,2*puff,0])
    scale([0.9,0.9,1])
    linear_extrude(height = vpd2)
    rotate([0,180,0])
       { offset(r = thicc, $fn=res)
         import(tis, center = true, $fn=res);        // shallow logo
       }
    */   
   } //end of objects to remove
}

  
//WAVERED RISING 
translate([1.5*bw,0,0])
rotate([65,-15,15])
difference()
{
  //start of initial object
  union()
    {
      // puffed bars, to give rounded corners on top
      linear_extrude(height = th)
      offset(r = puff, $fn=res)
      polygon(points=[ [0,0],[bw,0],[bw,td],[td,td],[td,wd],[0,wd] ]);
      
    }
  //end of initial object
    
  //start of object to remove
  union()
  {
    rotate([0,0,90])
    translate([-puff*0.35,-puff*0.35,th-vpd])
    linear_extrude(height = vpd)
       {
        text(vp0, font = txtfnt, size = ifs, valign = "top", halign = "left", $fn=res); // top text
       }
    translate([bw/1.5,0,th-vpd])
    scale([0.9,0.9,1])
    linear_extrude(height = vpd)
       {
        import(tid, center = true, $fn=res);                    // deep logo
       }
    translate([bw/1.5,2*puff,th-vpd2])
    scale([0.9,0.9,1])
    linear_extrude(height = vpd2)
       { offset(r = thicc, $fn=res)
         import(tis, center = true, $fn=res);                    // shallow logo 
       }
   // WAVERED
    translate([puff,bw/1.7,0])
    linear_extrude(height = vpd)
    rotate([0,180,90])
       {
        text("WAVERED", font = txtfnt, size = ifs, valign = "center", halign = "center", $fn=res); // top
       }
    translate([bw/1.7,puff,0])
    linear_extrude(height = vpd)
    rotate([0,180,180])
       {
        text("WAVERED", font = txtfnt, size = ifs, valign = "center", halign = "center", $fn=res); // top
       }
/*    translate([bw/1.5,0,0])
    scale([0.9,0.9,1])
    linear_extrude(height = vpd)
    rotate([0,180,0])
       {
        import(tid, center = true, $fn=res);        // deep logo
       }
    translate([bw/1.5,2*puff,0])
    scale([0.9,0.9,1])
    linear_extrude(height = vpd2)
    rotate([0,180,0])
       { offset(r = thicc, $fn=res)
         import(tis, center = true, $fn=res);        // shallow logo
       }
*/       
   } //end of objects to remove
}
