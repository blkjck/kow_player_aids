// Kings of War tokens version 0.2

sf = 1; //scale factor. set to 1 for mm and 25.4 for inches. 

ifs = 16; // Increments Number/text values font size
txtfnt = "Memo Std"; // Increments Number/text values font. Make sure to set it to something installed on your PC
vp0 = "0"; // token top text, 'victory points' being the usual
th = 3.2 ; // token height value
td = 25 ; // token diameter
vpd = 0.6 ; // token text and image depth
thicc = 0.2; // make the logo lines thicc
res = 500; // dimensional accuracy - larger is more fine detail 50 is small, 800 is large

// use tid only if only using one svg image. 
// use both if you want to stack two overlapping images at two depths.
// when stacking two, tis should be the bigger image, and tid the image you want to cut out of it
tis = "../icons/sun.svg"; // shallow token image on the bottom
tid = "../icons/citysilhouette_flipped.svg"; // deep token image on the bottom


// Calculated values - DO NOT EDIT DIRECTLY
vpd2 = 0.5*vpd; // secondary token image depth

for (i=[0,1,2], z=[1,1,2]) // make a series of tokens, one with each of these values
{
  let (vp0 = str(i))
  //global operations
  scale([sf,sf,sf])
  
  //object definitions
  translate([(td*1.25*((z*i+2*i+z-((z-1)*(i+z-1)*(i/2)))-1-i)),0,0])
  difference()
   {
    //start of initial object
    cylinder(h=th,r=td/2,$fn=res);
    //end of initial object
  
    //start of object to remove
    union()
    {
      translate([0,0,th-vpd])
      linear_extrude(height = vpd)
         {
          text(vp0, font = txtfnt, size = ifs, valign = "center", halign = "center", $fn=res);
         }
        translate([0,td/8,0])
         //shallow image
        //rotate([0,180,-10])
        linear_extrude(height = vpd2)
         //text(tis, font = txtfnt, size = ifs/7, valign = "top", halign = "center", $fn=res); // bottom text
         import(tis, center = true, $fn=res); // bottom image
         //deeper image
         translate([0,-2.2,0]) //needed some adjustment to line up for sun and city
         linear_extrude(height = vpd)
         offset(r = thicc, $fn=res)
         import(tid, center = true, $fn=res);
        
    //end of object to remove
    }
   }
}
;