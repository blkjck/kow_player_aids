// Variable length measuring widget with ticks all along at x increments, with a labelled circular thumb spot for holing it.

// variables 
scl = 25.4;      // X/Y scaling factor for the object. Openscad base is mm, so scl = 1 will be mm. 10 = cm. 25.4 = inches.
ot = 3.6;     //object thickness in mm. do multiples of 0.2mm for best layer compatibility. will not scale as above
ol = 8;     // total object length when all segments added together
sl = 2;      // segment length 
ow = sl/3;      //segment width in units as per scl
ff = "Roboto:style=Bold"; // font's font and style

res = 500;   // resolution of curves. 20 is low. 800 is high

// calculated for optimals - do not manually adjust
label = str(ol); //label - the text on the thumb spot
ts = ow/2;      //radius of the circle as per scl. Also used for aligning objects on it.
fs = ts;      //font size
dd = ot/3;     //detail depth
ns = ol/sl;   // number of segments

// flexi stick

for (i=[0:scl*sl:scl*(ol-sl)])
//for (i=[1])

translate([0,i*ot/scl,0])

rotate([90,0,0])
scale ([scl,scl,1])
union()
{
  difference()
  {
    union()
    {
      translate([ts,0,0])
        cube([sl-ts,ow,ot]);
      translate([ts,ts,0])    
        cylinder(h=ot,r=ts, center = false, $fn=res);
      translate([sl,ts,0])    
        cylinder(h=ot,r=ts, center = false, $fn=res);
    }
    union()
    {
  // swivel joint left
      translate([ts,ts,ot/2])    
        cylinder(h=ot/2,r=ts, center = false, $fn=res);
  // post hole left
      translate([ts,ts,0])    
        cylinder(h=ot,r=ts/2, center = false, $fn=res);
  // swivel joint right
      translate([sl,ts,0])    
        cylinder(h=ot/2,r=ts, center = false, $fn=res);

    }
  }
  difference()
  {
  // post w/ flange - right
    union()
    {
    // post
      color("blue")
      translate([sl,ts,0])    
        cylinder(h=ot,r=ts/2.1, center = false, $fn=res);
    // flange
      translate([sl,ts,0])    
        cylinder(h=ot/5,r=ts/2, center = false, $fn=res);
    } 
    // cut outs
    
    // vertical cut
    translate([sl-1/64,ts/2,0])
      cube([1/32,ts,ot/2]);
    // remove overhang
   // translate([sl-ts/2.5,ts/2,0])
     // cube([2*ts/2.5,ts/2.5-ts/3,ot/2]);
    
    // ring
    difference()
    {
    // post
      translate([sl,ts,0])    
        cylinder(h=ot,r=ts/2.5, center = false, $fn=res);
      translate([sl,ts,0])    
        cylinder(h=ot/5,r=ts/3, center = false, $fn=res);
    }
  }
}
// begin stick

//scale ([scl,scl,1])
//difference()
//{

//union()  
  //{ // positive extrustions
    //// main body
    //linear_extrude(height = ot ) polygon(points=[[0,0],[ol,0],[ol-ow,ow],[ow,ow] ]);
    
    //// thumb grip
    //translate ([ol/2,ts,0])     cylinder(ot,ts,ts,$fn=6);
    
    //// top marks if pvpnv = 1
    //translate ([ol/2,ts,ot]) 
    //linear_extrude(height = pvnv*dd) text(label,fs, halign = "center", valign = "center", font = ff, $fn=500);
 
    //for (t=[ti:ti:ol-ti])
    //translate ([t,0,ot])
    //linear_extrude(height = pvnv*dd) polygon(points=[[0,0],[tw,(2*ow)/3],[-1*(tw),(2*ow)/3]]);
    
  //}



//// negative extrusions - sink a smidge below the plane to ensure rendering cut outs correctly
//union()
  //{
    //// top marks
    //translate ([ol/2,ts,ot-dd]) 
    //linear_extrude(height = (1-pvnv)*dd) text(label,fs, halign = "center", valign = "center", font = ff, $fn=500);
 
    //for (t=[ti:ti:ol-ti])
    //translate ([t,0,ot-dd])
    //linear_extrude(height = dd) polygon(points=[[0,0],[tw,(2*ow)/3],[-1*(tw),(2*ow)/3]]);
    
    ////bottom marks
    //translate ([ol/2,ts,dd]) rotate([0,180,0])  
    //linear_extrude(height = (1-pvnv)*dd) text(label,fs, halign = "center", valign = "center", font = ff, $fn=500);

    //for (t=[ti:ti:ol-ti])
    //translate ([t,0,0])
    //linear_extrude(height = dd) polygon(points=[[0,0],[tw,2*ow/3],[-1*tw,2*ow/3]]);
  //}
//}