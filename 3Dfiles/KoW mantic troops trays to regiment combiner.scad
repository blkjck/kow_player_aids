// Kings of War unit combiner - 2x infantry troop to regiment

sf = 1; //scale factor. set to 1 for mm and 25.4 for inches. 

ifs = 5; // Increments Number/text values font size
txtfnt = "Memo Std"; // Increments Number/text values font. Make sure to set it to something installed on your PC
vp0 = "Troops to Regiment"; // tray top text, 'victory points' being the usual for tokens
th = 4.6 ; // tray height to top of lip
lh =  2.6 ; // lip height
td = 0.6 ; //  text and image depth
itw = 105;  // inner tray width
itl = 85; // inner tray length
lt = 1.5 ; // lip thickness
thicc = 0.2; // make the logo lines thicc when using an image and the offset function
res = 500; // dimensional accuracy - larger is more fine detail 50 is small, 800 is large
mer = 5; //mouse ear radius
meh = 0.3; //mouse ear height

// use tid only if only using one svg image. 
// use both if you want to stack two overlapping images at two depths.
// when stacking two, tis should be the bigger image, and tid the image you want to cut out of it
tid = "Infantry Troops to Regiment";; // tray image, deep. or text. Use a path for image, or string for text. 
                      // You will need to change the command below to either 'import' or 'text', as appropriate.
tis = "../icons/sun.svg"; // tray image, shallow

// Calculated values - DO NOT EDIT DIRECTLY
otw = itw + 2*lt; // outer tray width
otl = itl + 2*lt; // outer tray length

//global operations
scale([sf,sf,sf])
  
//object definitions
difference()
{
  union()
  {
  //initial tray
  cube([otw,otl,th]);
  // mouse ears
  for (x=[0,1], y=[0,1]) // make a series of mouse ears
  translate([x*otw,y*otl])
  cylinder(meh, r=mer); 
  //end of positive components
  }  
  //start of objects to remove
    // inner tray cutout
    translate([lt,lt,th-lh])
    cube([itw,itl,lh]);
    
    // text/logo cutout
    translate([otw/2,otl/2,th-lh-td])
    linear_extrude (height = td)
    text(tid, font = txtfnt, size = ifs, valign = "center", halign = "center", $fn=res); // deep  text
    //import(tid, center = true, $fn=res);                    // deep logo
    
    // notch for leader point
    translate([otw/2,0,0])
    cylinder(th,0,1,$fn=4);
    
  //end of objects to remove
}

