// Kings of War arc widget masters version

sf = 1; //scale factor. set to 1 for mm and 25.4 for inches. 

ifs = 5; // Increments Number/text values font size
txtfnt = "Memo Std"; // Increments Number/text values font. Make sure to set it to something installed on your PC
vp0 = "AUSTRALIAN"; // token top text, 'victory points' being the usual for tokens
th = 3.2 ; // token (object) height (thickness) value
td = 25 ; // token diameter. Not used for widget
vpd = 0.6 ; // token text and image depth
puff = 2.5; // radius of offset function for making widget have rounded top corners
bw = 50;  // base width of widget
wd = 25.4; // widget depth. 25.4mm = 1 inch
thicc = 0.2; // make the logo lines thicc
res = 500; // dimensional accuracy - larger is more fine detail 50 is small, 800 is large

// use tid only if only using one svg image. 
// use both if you want to stack two overlapping images at two depths.
// when stacking two, tis should be the bigger image, and tid the image you want to cut out of it
tid = "MASTERS 2023"; // deep token image, or text. Use a path for image, or string for text. 
                      // You will need to change the command below to either 'import' or 'text', as appropriate.
tis = "../icons/masters_token_clean.svg"; // shallow token image 

// Calculated values - DO NOT EDIT DIRECTLY
vpd2 = 0.8*vpd; // secondary token image depth
ho = (puff/2^0.5)-(puff-(puff/2^0.5)); // horizontal offset of puff to align with main edge. lots of trigonometry to get this formula!


//global operations
scale([sf,sf,sf])
  
//object definitions
difference()
{
  //start of initial object
  union()
    {
      // puffy part, to give rounded corners on top
      linear_extrude(height = th)
      offset(r = puff, $fn=res)
      polygon(points=[ [-(bw/2)+ho,puff],[(bw/2)-ho,puff],[(bw/2)-ho+(wd-2*puff),wd-puff],[-((bw/2)-ho+(wd-2*puff)),wd-puff]]);
      
      // chopped sharp top corners off to let rounded top corners come through. So really all this adds is sharp lower corners.
      linear_extrude(height = th)
      polygon(points=[ [-(bw/2),0],[(bw/2),0],[((bw/2)+wd)-3*puff,wd-3*puff],[((bw/2)+wd)-3*puff,wd],[-((bw/2)+wd-3*puff),wd],[-((bw/2)+wd-3*puff),wd-3*puff]]);
    }
  //end of initial object
    
  //start of object to remove
  union()
  {
    translate([0,20,th-vpd])
    linear_extrude(height = vpd)
       {
        text(vp0, font = txtfnt, size = ifs, valign = "center", halign = "center", $fn=res); // top text
       }
    translate([0,5,th-vpd])
    linear_extrude(height = vpd)
       {
        text(tid, font = txtfnt, size = ifs, valign = "center", halign = "center", $fn=res); // deep  text
        //import(tid, center = true);                    // deep logo
       }
    translate([0,13,th-vpd2])
    linear_extrude(height = vpd2)
       { offset(r = thicc, $fn=res)
         import(tis, center = true);                    // shallow logo 
         // text(tis, font = txtfnt, size = ifs, valign = "center", halign = "center", $fn=800); // shallow text
       }
   // bottom version
    translate([0,20,0])
    linear_extrude(height = vpd)
    rotate([0,180,0])
       {
        text(vp0, font = txtfnt, size = ifs, valign = "center", halign = "center", $fn=res); // top
       }
    translate([0,5,0])
    linear_extrude(height = vpd)
    rotate([0,180,0])
       {
        //import(tid, center = true);        // deep logo
        text(tid, font = txtfnt, size = ifs, valign = "center", halign = "center", $fn=res);  // deep text
       }
    translate([0,13,0])
    linear_extrude(height = vpd2)
    rotate([0,180,0])
       { offset(r = thicc, $fn=res)
         import(tis, center = true);        // shallow logo
         //text(tis, font = txtfnt, size = ifs, valign = "center", halign = "center", $fn=800);  // shallow text
       }
       
   } //end of objects to remove
}

