# UNOFFICIAL Kings of War Player Aids

Files to build your own UNOFFICIAL player aids for Kings of War, as well as pre-made ones. Created using all open-source tools - LibreOffice, Inkscape, Scribus and the Scribus Generator script.

**If you want to skip straight to the printable files, go look in the "pdfs" folder.**

If you want to play around with the files yourself, you'll need the following:

https://www.libreoffice.org/

https://inkscape.org/en/

https://www.scribus.net/

https://github.com/berteh/ScribusGenerator  

**Prerequisites for ScribusGenerator script:** python3, Tkinter  

On Ubuntu  

<code>$ sudo apt update</code> 
 
<code>$ sudo apt install python3 python3-tk</code>

**Notes:**

Export the CSV with | as a delimiter, and do not set any text quoting.

When using the ScribusGenerate script, make sure your delimiter is also a |, and ensure that your output directory is the same as where your input scribus project is stored. It seems to store the image variables as relative paths, and if you export into a different directory the images will not come through.  

Also ensure you choose "single file" as an output option, otherwise it seems that only the last row of the CSV's pdf will get generated.  

1) Build your card and card back once. 
2) Ensure a text box appears on the card somewhere containing "%VAR_NEXT-RECORD%".
3) Raise the %VAR_NEXT-RECORD% text box to be the highest level using the up arrow in the properties box.
4) Select all and group all the card elements together.
5) Item menu -> multiple duplicate. Make it repeat an additional 3 times to fill the page with 4 cards. 1.5mm vertical gap.
6) Select each card group and ungroup them in turn.
7) Remove the %VAR_NEXT-RECORD% text box from the last card. The script knows to go to the next record at the end of the single page automatically.

Tuckbox generated at https://andylei.github.io/paperbox/ - make sure you turn off all scaling, print at true size only!

### Icon key:  
**Shield**: Defensive ability.  

**Helmet**: Morale effect (nerve, waver, etc).  

**Arrow**: Ranged effect for the shooting phase.  

**Swords**: Melee effect.  

**Winged foot**: Movement ability.  

**Star**: Special. A spell or other timing that doesn't fit into the above.  

Mantic, Kings of War, Kings of War: Vanguard, and all associated characters, names, places and things are TM and (C) Mantic Entertainment Ltd 2018  
Used with permission. Visit http://www.manticgames.com or your friendly local gaming store and buy their stuff, they are an amazing fan driven company!
