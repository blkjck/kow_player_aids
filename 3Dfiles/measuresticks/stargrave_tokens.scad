// stargrave tokens

th = 1.8 ; // token height value
td = 20 ; // token diameter
vpd = 0.6 ; // token text and image depth
res = 6;  // resolution - dimensional accuracy - larger is finer detail. 6 applied to a cylinder is a hexagon. 
          // 8 octagon. For actual cylinders/text, 50 is small, 128 is large
gi = 0.60*td/2; //greater increment  for drawing to edges
tox = "../assets/toxin.svg"; // toxin svg
pc = "../assets/probabilitycurve.svg"; // probability curve icon svg
luck = "../assets/fortune.svg"; // fortune logo svg

// modules for reuse

module token (){
                difference()
                {
                //main solid
                cylinder(h=th,r=td/2,$fn=res); 
                //end of initial object
                
                //rings
                difference()
                {
                //upper
                // outer diameter
                translate([0,0,th-vpd])
                cylinder(h=vpd,r=td/2.3,$fn=res);
                // inner diameter
                translate([0,0,th-vpd])
                cylinder(h=vpd,r=td/2.8,$fn=res);
                }
                difference()
                {
                //lower
                // outer diameter
                cylinder(h=vpd,r=td/2.3,$fn=res);
                // inner diameter
                cylinder(h=vpd,r=td/2.8,$fn=res);
                }
                }
              }

module star (prongs = 1, stretch = 1, barthick=1) {
                      linear_extrude(height = vpd)
                      for (t=[1:1:prongs])
                      rotate (t*360/prongs/2)
                      polygon([ [-1*stretch*gi,-1*barthick*vpd/2],[-1*stretch*gi,barthick*vpd/2],[stretch*gi,barthick*vpd/2],[stretch*gi,-1*barthick*vpd/2] ]);
                      }
module jammed( stretch = 1, barthick=1, dotsize = 1) {
                      linear_extrude(height = vpd)
                      polygon([ [-1*barthick*td/13,stretch*td/30*gi],[0,stretch*gi*td/20],[barthick*td/13,stretch*td/30*gi],[0,-1*stretch*td/10] ]);
                      translate([0,-1*stretch*td/5])
                      cylinder(h=vpd,r=dotsize*td/15,$fn=res);
                      }
module toxin( stretch = 1, barthick=1, dotsize = 1) {
                      linear_extrude(height = vpd)
                      translate([0,td/35,0])
                      offset(r = vpd/3, $fn=500)
                      scale([0.0012*td,0.0012*td])
                      import(tox, center = true, $fn=500);
                      }                

module svglogo(svg=pc, bigness=0.0025*td, thicc=0, reso=500) {
                      linear_extrude(height = vpd)
                      translate([0,td/35,0])
                      offset(r = thicc, $fn=reso)
                      scale([bigness,bigness])
                      import(svg, center = true, $fn=reso);
                      }   

module ring (outerrad = 2, thick=1) {
                      difference(){
                      cylinder(h = vpd,r=outerrad, $fn=360);
                      cylinder(h=vpd,r=outerrad-thick,$fn=360);}
                      }                      
                      
//object definitions

// wounded
  translate([0,0,0])
  difference()
   {
    token();
    translate([0,0,th-vpd])
      star(2,0.8,4);  
    star(2,0.8,4);  
   }
;
 
// stunned
  translate([30,0,0])
  difference()
   {
    token();
    translate([0,0,th-vpd])
      star(5,1.2,2);   
    star(5,1.2,2);
   }
;


// jammed
  translate([60,0,0])
  difference()
   {
    token();
    translate([0,0,th-vpd])
      jammed();   
    jammed();
   }
;

// toxin
  translate([90,0,0])
  difference()
   {
    token();
    translate([0,0,th-vpd])
      toxin();   
    toxin();
   }
;   

// probability curve
  translate([130,0,0])
  scale([2,2,2])
  difference()
   {
    token();
    translate([0,0,th-vpd])
    svglogo(svg=pc,thicc=0.42);   
    //flip over when not active
    ring(td/3.5,1);
    rotate([0,0,-45])
    star(1,0.9,2);
   }
   
// fortune
  translate([180,0,0])
  scale([2,2,2])
  difference()
   {
    token();
    translate([0,-1,th-vpd])
    svglogo(svg=luck, bigness=0.025, thicc=0);   
    //flip over when not active
    ring(td/3.5,1);
    rotate([0,0,-45])
    star(1,0.9,2);
   }