Feel free to pull down copies of the .ods files in the data directory, and then create new ones for factions or cards that you could like to see put into the templates. Ensure the column names and order are not changed in the slightest, or they will not work with the templates.

Name them the same as the existing file but with the new faction name or set in the file name.

Then either email them through to me, or create a merge request with the new data file.

If accepted your name will get added to the contributor list at the end of the readme (if you want - just let me know either way).